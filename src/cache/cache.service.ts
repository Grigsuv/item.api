import { Injectable } from '@nestjs/common';

@Injectable()
export class CacheService {
  private cache = {};

  update(key: string, value: any): void {
    this.cache[key] = value;
  }

  get(key: string): any {
    return this.cache[key] || null;
  }
}
