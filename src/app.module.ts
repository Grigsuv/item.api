import { Module } from '@nestjs/common';

import { ExchangeModule } from './exchange/exchange.module';
import { CacheModule } from './cache/cache.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [ExchangeModule, CacheModule, ScheduleModule.forRoot()],
  controllers: [],
  providers: [],
})
export class AppModule {}
