import { ExchangeService } from './exchange.service';
import { CacheService } from '../cache/cache.service';
import axios from 'axios';
import * as parserXml from 'xml2js';

describe('ExchangeService', () => {
  let service: ExchangeService;
  let cacheService: CacheService;

  beforeEach(async () => {
    service = new ExchangeService(cacheService);
  });

  it('init should return the result from getRatesFromAPI', async () => {
    jest.spyOn(service, 'getRatesFromAPI').mockResolvedValueOnce({ USD: 1 });
    expect(await service.init()).toStrictEqual({
      USD: 1,
    });
  });

  it('getRatesFromAPI should return correct values', async () => {
    const mockAPIResult = `<gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01" xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">
                            <gesmes:subject>Reference rates</gesmes:subject>
                            <gesmes:Sender>
                            <gesmes:name>European Central Bank</gesmes:name>
                            </gesmes:Sender>
                            <Cube>
                            <Cube time="2020-08-27">
                            <Cube currency="USD" rate="1.1806"/>
                            <Cube currency="JPY" rate="125.34"/>
                            <Cube currency="BGN" rate="1.9558"/>
                            <Cube currency="CZK" rate="26.276"/>
                            <Cube currency="DKK" rate="7.4429"/>
                            <Cube currency="GBP" rate="0.89505"/>
                            <Cube currency="HUF" rate="355.98"/>
                            <Cube currency="PLN" rate="4.3993"/>
                            <Cube currency="RON" rate="4.8415"/>
                            <Cube currency="SEK" rate="10.3138"/>
                            <Cube currency="CHF" rate="1.075"/>
                            <Cube currency="ISK" rate="163.5"/>
                            <Cube currency="NOK" rate="10.5178"/>
                            <Cube currency="HRK" rate="7.5262"/>
                            <Cube currency="RUB" rate="88.64"/>
                            <Cube currency="TRY" rate="8.6665"/>
                            <Cube currency="AUD" rate="1.6291"/>
                            <Cube currency="BRL" rate="6.601"/>
                            <Cube currency="CAD" rate="1.5521"/>
                            <Cube currency="CNY" rate="8.1323"/>
                            <Cube currency="HKD" rate="9.1497"/>
                            <Cube currency="IDR" rate="17380.08"/>
                            <Cube currency="ILS" rate="3.9779"/>
                            <Cube currency="INR" rate="87.2655"/>
                            <Cube currency="KRW" rate="1399.75"/>
                            <Cube currency="MXN" rate="25.9205"/>
                            <Cube currency="MYR" rate="4.9261"/>
                            <Cube currency="NZD" rate="1.7799"/>
                            <Cube currency="PHP" rate="57.384"/>
                            <Cube currency="SGD" rate="1.6128"/>
                            <Cube currency="THB" rate="36.87"/>
                            <Cube currency="ZAR" rate="20.0275"/>
                            </Cube>
                            </gesmes:Envelope>`;

    const mockParsingResult = {
      'gesmes:Envelope': {
        $: {
          'xmlns:gesmes': 'http://www.gesmes.org/xml/2002-08-01',
          xmlns: 'http://www.ecb.int/vocabulary/2002-08-01/eurofxref',
        },
        'gesmes:subject': ['Reference rates'],
        'gesmes:Sender': [
          {
            'gesmes:name': ['European Central Bank'],
          },
        ],
        Cube: [
          {
            Cube: [
              {
                $: {
                  time: '2020-08-27',
                },
                Cube: [
                  {
                    $: {
                      currency: 'USD',
                      rate: '1.1806',
                    },
                  },
                  {
                    $: {
                      currency: 'JPY',
                      rate: '125.34',
                    },
                  },
                  {
                    $: {
                      currency: 'BGN',
                      rate: '1.9558',
                    },
                  },
                ],
              },
            ],
          },
        ],
      },
    };
    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: mockAPIResult });
    jest
      .spyOn(parserXml, 'parseStringPromise')
      .mockResolvedValueOnce(mockParsingResult);
    const resultFromAPI = await service.getRatesFromAPI();

    expect(resultFromAPI).toStrictEqual({
      USD: 1.1806,
      JPY: 125.34,
      BGN: 1.9558,
    });
  });

  it('convert should return correct value', async () => {
    const currentCurrencyRate = 1.24,
      newCurrencyRate = 1.8,
      currentCurrency = 'EUROS',
      targetCurrency = 'USD',
      price = 20;

    jest.spyOn(service, 'getRate').mockResolvedValueOnce(currentCurrencyRate);
    jest.spyOn(service, 'getRate').mockResolvedValueOnce(newCurrencyRate);

    expect(
      await service.convert(price, targetCurrency, currentCurrency),
    ).toStrictEqual({
      price: (price / currentCurrencyRate) * newCurrencyRate,
      currency: targetCurrency,
    });
  });
});
