export enum Currencies {
  Euros = 'European Union',
  USD = 'United States',
  RUB = 'Russia',
}
