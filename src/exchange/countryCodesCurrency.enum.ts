export enum CountryCodesCurrencyEnum {
  US = 'USD',
  EU = 'EUROS',
  RU = 'RUB',
}
