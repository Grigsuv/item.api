import { IsEnum, IsNumberString, MinLength } from 'class-validator';
import { Currencies } from './currency.enum';

export class ItemPriceDto {
  @IsNumberString()
  @MinLength(1)
  price: number;

  @IsEnum(Currencies)
  currency: string;
}
