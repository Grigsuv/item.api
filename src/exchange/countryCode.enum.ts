export enum CountryCodes {
  'United states' = 'US',
  'Europe' = 'EU',
  'Republic of Russia' = 'RU',
}
