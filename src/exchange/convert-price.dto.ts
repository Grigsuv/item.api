import { IsEnum } from 'class-validator';
import { ItemPriceDto } from './item-price.dto';
import { CountryCodes } from './countryCode.enum';

export class ConvertPriceDto extends ItemPriceDto {
  @IsEnum(CountryCodes)
  countryCode: string;

  currentCurrency: string;

  targetCurrency: string;
}
