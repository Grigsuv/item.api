import { Injectable, Logger } from '@nestjs/common';
import { CacheService } from '../cache/cache.service';
import { parseStringPromise } from 'xml2js';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ConvertResultDto } from './convert-result.dto';
import axios from 'axios';

@Injectable()
export class ExchangeService {
  private cacheKey = 'RATES';

  constructor(private cacheService: CacheService) {}
  private readonly logger = new Logger(ExchangeService.name);

  init() {
    this.logger.log('Called getRatesFromAPIAndCache for initialization');

    return this.getRatesFromAPI();
  }

  @Cron(CronExpression.EVERY_MINUTE)
  handleCron() {
    this.logger.debug('Called getRatesFromAPIAndCache');

    this.getRatesFromAPIAndCache();
  }
  private async getRatesFromAPIAndCache() {
    const rates = await this.getRatesFromAPI();
    this.cacheService.update(this.cacheKey, rates);
    return rates;
  }
  async getRatesFromAPI() {
    const result = await axios.get(
      `https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml`,
    );

    const parsedXMLResult = await parseStringPromise(result.data);
    if (parsedXMLResult.hasOwnProperty('gesmes:Envelope')) {
      const {
        'gesmes:Envelope': {
          Cube: [
            {
              Cube: [{ Cube: latestCurrency }],
            },
          ],
        },
      } = parsedXMLResult;

      return latestCurrency.reduce((acc, curr) => {
        const [{ currency, rate }] = Object.values(curr);

        return {
          ...acc,
          [currency]: Number(rate),
        };
      }, {});
    } else {
      throw new Error('Unexpected API result');
    }
  }

  async getRate(currency: string): Promise<number> {
    if (currency.toUpperCase() === 'EUROS') return 1;
    let rates = this.cacheService.get(this.cacheKey);
    if (!rates) rates = await this.getRatesFromAPIAndCache();
    return rates[currency];
  }

  async convert(
    price: number | string,
    targetCurrency: string,
    currentCurrency = 'USD',
  ): Promise<ConvertResultDto> {
    if (typeof price === 'string') price = Number(price);

    const currentCurrencyRate = await this.getRate(currentCurrency);
    const newCurrencyRate = await this.getRate(targetCurrency);

    return {
      price: (price / currentCurrencyRate) * newCurrencyRate,
      currency: targetCurrency,
    };
  }
}
