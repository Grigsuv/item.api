import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { Currencies } from './currency.enum';
import { CountryCodesCurrencyEnum } from './countryCodesCurrency.enum';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  private readonly countryNamesReversed;
  private readonly countryCodesMap;

  constructor() {
    this.countryCodesMap = CountryCodesCurrencyEnum;
    this.countryNamesReversed = Object.entries(Currencies).reduce(
      (acc, [currency, nameOfCurrency]) => {
        return { ...acc, [nameOfCurrency]: currency };
      },
      {},
    );
  }
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }

    const object = plainToClass(metatype, value);
    const errors = await validate(object);
    if (errors.length > 0) {
      throw new BadRequestException('Validation failed');
    }
    const { price, countryCode, currency } = value;
    const currentCurrency = this.countryNamesReversed[currency];
    const targetCurrency = this.countryCodesMap[countryCode];

    return {
      ...value,
      price: Number(price),
      currentCurrency,
      targetCurrency,
    };
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Number, Object];
    return !types.includes(metatype);
  }
}
