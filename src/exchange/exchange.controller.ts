import { Controller, Get, Query, UsePipes } from '@nestjs/common';
import { ExchangeService } from './exchange.service';
import { ConvertPriceDto } from './convert-price.dto';
import { ConvertResultDto } from './convert-result.dto';
import { ValidationPipe } from './validateAndTransform';

@Controller('exchange')
export class ExchangeController {
  constructor(private exchangeService: ExchangeService) {
    this.exchangeService.init();
  }

  @Get()
  @UsePipes(new ValidationPipe())
  async convertPrice(
    @Query() converter: ConvertPriceDto,
  ): Promise<ConvertResultDto> {
    const { price, currentCurrency, targetCurrency } = converter;
    return this.exchangeService.convert(price, targetCurrency, currentCurrency);
  }
}
